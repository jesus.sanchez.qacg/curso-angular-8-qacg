# Curso de Angular 8 - QACG

## Bienvenido al Curso de Angular 8 para Quality & Knowledge on IT Services.

Contaremos con cuatro carpetas que utilizaremos: 
	
	* Herramientas
	* Manuales
	* Proyecto
	* Videos
	
### Herramientas
Aquí encontraremos las diversas herramientas que utilizaremos durante el curso.

### Manuales
Les compartiremos una serie de manuales y libros que les ayudaran a reforzar sus conocimientos adquiridos y podrán usarlos posteriormente como consulta.

### Proyecto
Iremos publicando los avances del código fuente que hagamos durante el curso sobre el proyecto.

### Videos
Les compartimos las sesiones grabadas