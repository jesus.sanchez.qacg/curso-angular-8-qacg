@echo off
echo "Resetting License Evaluator of WebStorm2019.3"
del C:\Users\%username%\.WebStorm2019.3\config\eval /f
del C:\Users\%username%\.WebStorm2019.3\config\options\other.xml /f
reg delete HKEY_CURRENT_USER\Software\JavaSoft\Prefs\jetbrains\webstorm /f
powershell -command "& {Get-ChildItem C:\Users\%username%\.WebStorm2019.3\* | Where{$_.LastWriteTime -gt (Get-Date).AddDays(-2)}}"
echo "Finalize"
pause